<?php
/**
 * @file
 *   Installation and configuration tasks of TMGMT Translation Server.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function hermes_form_install_configure_form_alter(&$form, $form_state) {

  // Set the flag which is used in hermes_install_tasks() to
  // determine if to override the cache setting to use DB cache.
  variable_set('hermes_use_db_cache', TRUE);

  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}

/**
 * Implements hook_install_tasks().
 */
function hermes_install_tasks() {

  // At the language capabilities step we need a persistent cache to be
  // present, otherwise ajax form will not work.
  if (variable_get('hermes_use_db_cache', FALSE)) {
    global $conf;
    $conf['cache_default_class'] = 'DrupalDatabaseCache';
  }

  return array(
    'hermes_install_auto_config' => array(
      'display_name' => st('Auto configuration'),
      'type' => 'normal',
      'run' => INSTALL_TASK_RUN_IF_REACHED,
    ),
    'hermes_ds_settings_form' => array(
      'display_name' => st('Setup Directory Server'),
      'type' => 'form',
    ),
    'hermes_language_capabilities_form' => array(
      'display_name' => st('Configure language capabilities'),
      'type' => 'form',
    ),
    'hermes_install_cleanup' => array(
      'display_name' => st('Cleanup'),
      'type' => 'normal',
      'run' => INSTALL_TASK_RUN_IF_REACHED,
    ),
  );
}

/**
 * Runs tasks needed to successfully run following install tasks.
 */
function hermes_install_auto_config() {
  // We need to unset translator info as otherwise it will not pickup translator
  // info from modules enabled by the call below.
  $info = &drupal_static('_tmgmt_plugin_info');
  unset($info['translator']);

  module_enable(array('tmgmt_local'));

  // Allow visitors to register. It still requires email verification.
  variable_set('user_register', USER_REGISTER_VISITORS);

}

/**
 * Install task to configure DS connection.
 */
function hermes_ds_settings_form($form, &$form_state) {

  $admin_theme = variable_get('admin_theme');
  $settings = array(
    'ajaxPageState' => array(
      'theme' => $admin_theme,
      'theme_token' => drupal_get_token($admin_theme),
    ),
  );
  drupal_add_js($settings, 'setting');

  // Determine which authentication action has been selected.
  $auth_action = NULL;
  if (isset($form_state['values']['auth_action'])) {
    $auth_action = $form_state['values']['auth_action'];
  }

  // Determine if we have a new registration.
  $new_registration = FALSE;
  if (isset($form_state['new_registration'])) {
    $new_registration = $form_state['new_registration'];
  }

  /**
   * @var TMGMTDirectoryServerController $ds_controller
   */
  $ds_controller = entity_get_controller('tmgmt_directory_server');
  $ds = $ds_controller->loadByUrl(variable_get('tmgmt_ds_url', TMGMT_SERVER_DEFAULT_DS_URL));

  $form['auth'] = array(
    '#type' => 'fieldset',
    '#title' => t('Directory Server'),
    '#prefix' => '<div id="tmgmt-ds-auth-wrapper">',
    '#suffix' => '</div>',
    '#description' => t('Registering your Translation Server at the Directory server is optional, however highly recommended as your services will be automatically offered to the network of all the Translation Clients registered at the Directory Server.'),
  );

  // If we just registered or we have DS do not display radios to choose
  // auth action.
  if (!$new_registration && empty($ds)) {
    $form['auth']['auth_action'] = array(
      '#type' => 'radios',
      '#title' => t('Directory server authentication'),
      '#options' => array(
        'login' => t('I already have an account at the Directory Server, I will authenticate with my login and password.'),
        'register' => t('I do not have an account at the Directory Server, need to register.'),
        'none' => t('I do not want to connect to the Directory server.'),
      ),
      '#ajax' => array(
        'callback' => 'hermes_ds_settings_form_ajax',
        'wrapper' => 'tmgmt-ds-auth-wrapper',
      ),
    );
  }

  if (!empty($ds) || $auth_action == 'none') {

    $form['auth']['continue'] = array(
      '#type' => 'submit',
      '#value' => t('Continue'),
      '#weight' => 10,
    );

    if (!empty($ds)) {
      $form['auth']['info'] = array(
        '#markup' => '<div class="messages status">' .
            t('Directory Server configured, continue to the next step.') . '</div>'
      );
    }

    return $form;
  }
  elseif (empty($auth_action)) {
    return $form;
  }

  $form['auth']['ds_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Directory Server URL'),
    '#default_value' => variable_get('tmgmt_ds_url', TMGMT_SERVER_DEFAULT_DS_URL),
    '#description' => t('Please enter the URL of the Directory Server.'),
    '#required' => TRUE,
    // When we have new registration, disable this field.
    '#disabled' => $new_registration,
  );

  // We are going for login action.
  if ($auth_action == 'login') {
    if ($new_registration) {
      $form['auth']['info'] = array(
        '#markup' => '<div class="messages status">' .
            t('Your account at the Directory Server was created. To finish the registration process we have sent you an email with further instructions. Upon completion please authenticate your web site at the Directory server using your Directory Server user name and password that can be entered below.') . '</div>'
      );
    }
    $form['auth']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('User name'),
      '#required' => TRUE,
      '#description' => t('Enter your Directory Server user name.'),
    );
    $form['auth']['pass'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
      '#required' => TRUE,
      '#description' => t('Enter your Directory Server password.'),
    );
    $form['auth']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Authenticate'),
      '#validate' => array('hermes_ds_settings_authenticate_validate'),
      '#submit' => array('hermes_ds_settings_authenticate_submit'),
      '#ajax' => array(
        'callback' => 'hermes_ds_settings_form_ajax',
        'wrapper' => 'tmgmt-ds-auth-wrapper',
      ),
    );
  }
  // We need to register first.
  elseif ($auth_action == 'register') {
    $form['auth']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('User name'),
      '#required' => TRUE,
      '#description' => t('Enter your user name that will be used to login at the Directory Server.'),
    );
    $form['auth']['mail'] = array(
      '#type' => 'textfield',
      '#title' => t('E-mail'),
      '#required' => TRUE,
      '#description' => t('Enter your email address where further instructions to finalize the registration process at the Directory Server will be sent.'),
    );
    $form['auth']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Register at Directory Server'),
      '#validate' => array('hermes_ds_settings_register_validate'),
      '#submit' => array('hermes_ds_settings_register_submit'),
      '#ajax' => array(
        'callback' => 'hermes_ds_settings_form_ajax',
        'wrapper' => 'tmgmt-ds-auth-wrapper',
      ),
    );
  }

  return $form;
}

/**
 * Validator for user DS registration operation.
 *
 * It sends the registration request to the DS.
 */
function hermes_ds_settings_register_validate($form, &$form_state) {
  $values = $form_state['values'];
  $ds_connector = new TMGMTServerDSConnector($values['ds_url']);

  try {
    $ds_connector->registerDSUser($values['mail'], $values['name']);
  }
  catch (TMGMTRemoteException $e) {
    form_set_error('', $e->getMessage());
  }
}

/**
 * Submit for user DS registration operation.
 *
 * It only sets workflow flags into form state. All the registration logic is in
 * the validate callback.
 */
function hermes_ds_settings_register_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Set DS url.
  variable_set('tmgmt_ds_url', $form_state['values']['ds_url']);

  // Set flags to control workflow in tmgmt_client_ds_checkout_authenticate_form()
  $form_state['values']['auth_action'] = 'login';
  $form_state['new_registration'] = TRUE;
}

/**
 * Validate callback for TS authentication action at DS.
 *
 * Sends DS user login data together with TS url to DS. In response it gets
 * DS key and stores it the form state for further processing in the submit.
 */
function hermes_ds_settings_authenticate_validate($form, &$form_state) {
  $values = $form_state['values'];
  // Provide own url without the trailing slash.
  $url = rtrim(url('', array('absolute' => TRUE)), '/');
  $ds_connector = new TMGMTServerDSConnector($values['ds_url']);
  $response = NULL;

  try {
    $form_state['key_info'] = $ds_connector->createTS($values['name'], $values['pass'], $url);
  }
  catch (TMGMTRemoteException $e) {
    form_set_error('', $e->getMessage());
  }

}

/**
 * Stores DS key received by service call in the validate callback.
 */
function hermes_ds_settings_authenticate_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $values = $form_state['values'];

  // Set DS url.
  variable_set('tmgmt_ds_url', $values['ds_url']);

  /**
   * @var TMGMTDirectoryServerController $ds_controller
   */
  $ds_controller = entity_get_controller('tmgmt_directory_server');
  $ds = $ds_controller->create(array(
    'url' => $values['ds_url'],
  ));
  $ds_controller->save($ds);

  tmgmt_auth_receiver_controller()->receive($form_state['key_info'], 'tmgmt_directory_server', $ds->sid, TRUE);

  tmgmt_server_ds_settings_test();
  tmgmt_server_sync_ds_lang_definitions(1);
}

/**
 * Ajax callback for DS authorisation form part.
 */
function hermes_ds_settings_form_ajax($form, &$form_state) {
  return $form['auth'];
}

/**
 * Form submit callback.
 */
function hermes_ds_settings_form_submit($form, &$form_state) {
  // Do nothing, all was done via following ajax calls:
  // - hermes_ds_settings_register_validate()
  // - hermes_ds_settings_register_submit()
  // - hermes_ds_settings_authenticate_validate()
  // - hermes_ds_settings_authenticate_submit()
}

/**
 * Allows user to configure initial language capabilities of the server
 * as well as language skills of the admin user.
 */
function hermes_language_capabilities_form($form, &$form_state) {

  field_info_cache_clear();

  $admin_theme = variable_get('admin_theme');
  $settings = array(
    'ajaxPageState' => array(
      'theme' => $admin_theme,
      'theme_token' => drupal_get_token($admin_theme),
    ),
  );
  drupal_add_js($settings, 'setting');

  $form['demo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Setup demo Translation Server'),
    '#description' => t('This option provides possibility to setup Translation Server that works out of the box. The setup however is not suitable for production servers, so use it only to try things out.'),
    // Note that we need this checked in default to be able to run tests.
    '#default_value' => TRUE,
  );

  $form['language_capabilities'] = array(
    '#type' => 'fieldset',
    '#title' => t('Translation Server language capabilities'),
    '#description' => t('Define your server language capabilities using the form below. The selected language pairs will be also added to the admin user account as its translation skills.'),
    '#states' => array(
      'invisible' => array(
        ':input[name="demo"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form_state['list_all_languages'] = TRUE;

  // Set UID 1 user into form state.
  $account = user_load(1);
  $form_state['user'] = $account;

  $langcode = entity_language('user', $account);
  field_attach_form('user', $account, $form['language_capabilities'], $form_state, $langcode);
  unset($form['language_capabilities']['remote_client_addressfield']);

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
  );

  return $form;
}

function hermes_language_capabilities_form_submit($form, &$form_state) {

  // If user has not selected language capabilities, do nothing.
  if (isset($form_state['values']['tmgmt_translation_skills']['und'][0]) && (
      $form_state['values']['tmgmt_translation_skills']['und'][0]['language_from'] == '_none' || $form_state['values']['tmgmt_translation_skills']['und'][0]['language_to'] == '_none'
  )) {
    return;
  }
  // If user does not request demo install, do nothing.
  elseif (empty($form_state['values']['demo'])) {
    return;
  }

  $account = $form_state['user'];
  $category = 'account';
  // Remove unneeded values.
  form_state_values_clean($form_state);

  // Before updating the account entity, keep an unchanged copy for use with
  // user_save() later. This is necessary for modules implementing the user
  // hooks to be able to react on changes by comparing the values of $account
  // and $edit.
  $account_unchanged = clone $account;

  entity_form_submit_build_entity('user', $account, $form, $form_state);

  // Populate $edit with the properties of $account, which have been edited on
  // this form by taking over all values, which appear in the form values too.
  $edit = array_intersect_key((array) $account, $form_state['values']);

  user_save($account_unchanged, $edit, $category);
  $form_state['values']['uid'] = $account->uid;

  drupal_set_message(t('Please visit the <a href="@url">Directory Server status page</a> for further info.',
    array('@url' => url('admin/directory-server'))), 'warning');
}

function hermes_install_cleanup() {
  variable_del('hermes_use_db_cache');
}
